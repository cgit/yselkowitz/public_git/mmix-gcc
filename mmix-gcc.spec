#global bootstrap 1

%global gcc_major 5
%global gcc_minor 2
%global gcc_micro 0
%global gcc_branch %{gcc_major}.%{gcc_minor}
%global gcc_version %{gcc_major}.%{gcc_minor}.%{gcc_micro}
# Note, gcc_release must be integer, if you want to add suffixes to
# %%{release}, append them after %%{gcc_release} on Release: line.
%global gcc_release 1

%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Name:           mmix-gcc
Version:        %{gcc_version}
Release:        %{gcc_release}%{?dist}
Summary:        GCC cross-compiler for MMIX

License:        GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions
Group:          Development/Languages
URL:            http://gcc.gnu.org
Source0:        ftp://gcc.gnu.org/pub/gcc/releases/gcc-%{gcc_version}/gcc-%{gcc_version}.tar.bz2
# Fedora-specific patches
Patch1000:      gcc-textdomain.patch

BuildRequires:  texinfo
BuildRequires:  mmix-binutils
%if ! 0%{?bootstrap}
BuildRequires:  mmix-newlib
%endif
BuildRequires:  gmp-devel
BuildRequires:  mpfr-devel
BuildRequires:  libmpc-devel
%if 0%{?fedora} || 0%{?rhel} >= 7
BuildRequires:  libstdc++-static
%endif
%if 0%{?fedora} >= 20
BuildRequires:  isl-devel >= 0.12.1
%endif
BuildRequires:  zlib-devel
BuildRequires:  flex
BuildRequires:  gettext

Requires:       mmix-binutils
%if ! 0%{?bootstrap}
Requires:       mmix-newlib
%endif

%description
Cygwin cross-compiler (GCC) suite.


%prep
%setup -q -n gcc-%{gcc_version}
%patch1000 -p1

echo %{gcc_version} > gcc/BASE-VER
echo 'MMIX Copr %{gcc_version}-%{gcc_release}' > gcc/DEV-PHASE


%build
mkdir -p build
pushd build

CC="%{__cc} ${RPM_OPT_FLAGS}" \
../configure \
  --prefix=%{_prefix} \
  --mandir=%{_mandir} \
  --infodir=%{_infodir} \
  --build=%_build --host=%_host \
  --target=mmix \
  --with-gnu-as --with-gnu-ld --verbose \
  --with-newlib \
  --with-system-zlib \
  --disable-shared --enable-static \
  --enable-multilib \
  --without-included-gettext \
  --enable-version-specific-runtime-libs \
%if 0%{?fedora} >= 20
  --enable-graphite \
%endif
  --enable-languages="c,c++" \
  --enable-lto --disable-symvers \
  --disable-libatomic --disable-libgomp --disable-libitm \
  --disable-libstdcxx-pch --disable-libcc1 \
%if 0%{?bootstrap}
  --disable-libssp --disable-libquadmath --disable-libstdc++-v3 \
  --without-headers \
%else
  --enable-libssp \
%endif
  --with-bugurl=https://copr.fedoraproject.org/coprs/yselkowitz/mmix/

make %{?_smp_mflags} all

popd


%install
pushd build
make DESTDIR=$RPM_BUILD_ROOT install
popd

# These files conflict with existing installed files.
rm -rf $RPM_BUILD_ROOT%{_infodir}
rm -f $RPM_BUILD_ROOT%{_mandir}/man7/*

# Not sure these are useful on statically-linked targets
rm -f $RPM_BUILD_ROOT%{_prefix}/lib/gcc/mmix/%{version}/*-gdb.py
rm -f $RPM_BUILD_ROOT%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/*-gdb.py
rm -fr $RPM_BUILD_ROOT%{_datadir}/gcc-%{version}/python/libstdcxx/

# Don't want the *.la files.
find $RPM_BUILD_ROOT -name '*.la' -delete

%find_lang mmix-gcc
%find_lang mmix-cpplib
cat mmix-cpplib.lang >> mmix-gcc.lang


%files -f mmix-gcc.lang
%doc gcc/README* gcc/COPYING*
%{_bindir}/mmix-cpp
%{_bindir}/mmix-gcc
%{_bindir}/mmix-gcc-%{version}
%{_bindir}/mmix-gcc-ar
%{_bindir}/mmix-gcc-nm
%{_bindir}/mmix-gcc-ranlib
%{_bindir}/mmix-gcov
%{_bindir}/mmix-gcov-tool
%{_bindir}/mmix-g++
%{_bindir}/mmix-c++
%dir %{_prefix}/lib/gcc/mmix
%dir %{_prefix}/lib/gcc/mmix/%{version}
%{_prefix}/lib/gcc/mmix/%{version}/crtbegin.o
%{_prefix}/lib/gcc/mmix/%{version}/crtend.o
%{_prefix}/lib/gcc/mmix/%{version}/crti.o
%{_prefix}/lib/gcc/mmix/%{version}/crtn.o
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/crtbegin.o
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/crtend.o
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/crti.o
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/crtn.o
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/libgcc.a
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/libgcov.a
%if ! 0%{?bootstrap}
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/libssp.a
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/libssp_nonshared.a
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/libstdc++.a
%{_prefix}/lib/gcc/mmix/%{version}/gnuabi/libsupc++.a
%endif
%{_prefix}/lib/gcc/mmix/%{version}/libgcc.a
%{_prefix}/lib/gcc/mmix/%{version}/libgcov.a
%if ! 0%{?bootstrap}
%{_prefix}/lib/gcc/mmix/%{version}/libssp.a
%{_prefix}/lib/gcc/mmix/%{version}/libssp_nonshared.a
%{_prefix}/lib/gcc/mmix/%{version}/libstdc++.a
%{_prefix}/lib/gcc/mmix/%{version}/libsupc++.a
%endif
%{_prefix}/lib/gcc/mmix/%{version}/include/
%{_prefix}/lib/gcc/mmix/%{version}/include-fixed/
%dir %{_prefix}/lib/gcc/mmix/%{version}/install-tools
%{_prefix}/lib/gcc/mmix/%{version}/install-tools/*
%{_prefix}/lib/gcc/mmix/%{version}/plugin/
%{_libexecdir}/gcc/mmix/%{version}/cc1
%{_libexecdir}/gcc/mmix/%{version}/cc1plus
%{_libexecdir}/gcc/mmix/%{version}/collect2
%dir %{_libexecdir}/gcc/mmix/%{version}/install-tools
%{_libexecdir}/gcc/mmix/%{version}/install-tools/*
%{_libexecdir}/gcc/mmix/%{version}/liblto_plugin.so*
%{_libexecdir}/gcc/mmix/%{version}/lto1
%{_libexecdir}/gcc/mmix/%{version}/lto-wrapper
%{_libexecdir}/gcc/mmix/%{version}/plugin/
%{_mandir}/man1/mmix-cpp.1*
%{_mandir}/man1/mmix-gcc.1*
%{_mandir}/man1/mmix-gcov.1*
%{_mandir}/man1/mmix-g++.1*


%changelog
* Wed Aug 19 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 5.2.0-1
- Initial RPM release.
